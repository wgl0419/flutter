import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PageController controller = PageController(initialPage: 0);
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: PageView.builder(
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return Container(
                color: Colors.red,
                child: Center(
                  child: Text(
                    '${index}',
                    textScaleFactor: 5,
                  ),
                ),
              );
            }),
      ),
    );
  }
}
