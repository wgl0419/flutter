import 'package:flutter/material.dart';
import 'package:shopping_cart/model/shppping_cart.dart';
import 'package:shopping_cart/page/product_list_page.dart';
import 'package:shopping_cart/page/shopping_cart_page.dart';
import 'package:provider/provider.dart';

void main() => runApp(ChangeNotifierProvider(
      create: (context) => ShoppingCart(),
      child: MyApp(),
    ));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'shopping cart app',
      initialRoute: '/',
      routes: {
        '/': (context) => ProductListPage(),
        '/cart': (context) => ShoppingCartPage(),
      },
    );
  }
}
