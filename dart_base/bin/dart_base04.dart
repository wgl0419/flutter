//同步
import 'dart:io';

Future<String> getNetworkData() {
  return new Future(() {
    sleep(Duration(seconds: 2));
    return 'tangguodalong';
    // throw Exception('网络错误');
  });
}

//Future 已完成(成功、失败) 未完成
//匿名函数 箭头函数
//await
void main(List<String> args) async {
  print('start');
  //Future<String> f = getNetworkData();
  var name = await getNetworkData();
  // f.then((value) => print(value)).catchError((e) {
  //   print(e);
  // });
  print(name);
  print('end');
}
