import 'package:flutter/material.dart';
import 'package:provider_flutter/model/cart_model.dart';
import 'package:provider_flutter/page/cart.dart';
import 'package:provider_flutter/page/catalog.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CartModel(),
      child: MaterialApp(
        title: '数据共享',
        initialRoute: '/',
        routes: {
          '/': (context) => CatalogPage(),
          '/cart': (context) => CartPage(),
        },
      ),
    );
  }
}
